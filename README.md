# Query Manager

Send queries to maze agents, and display results at port 9090.

## Requirements

- [Java 1.8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
- [Apache Maven](https://maven.apache.org/download.cgi/)

## To run

Navigate to the folder in a terminal/shell and type: `mvn spring-boot:run`

On a browser, navigate to `http://localhost:9010/`

Register an agent [info here](https://gitlab.com/mams-ucd/atac-2021/README.md)

Send a query. NB it is case sensitive, e.g. 'Bedroom' or 'Closet'
