package perfectfield.sim.querymanager.agents;

public class AgentNotFoundException extends Throwable {
    public AgentNotFoundException(String s) {
        super(s);
    }
}
