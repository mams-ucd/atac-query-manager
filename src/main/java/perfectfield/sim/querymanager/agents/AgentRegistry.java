package perfectfield.sim.querymanager.agents;

import org.springframework.stereotype.Component;
import perfectfield.sim.querymanager.queue.NullQuery;
import perfectfield.sim.querymanager.queue.Query;
import perfectfield.sim.querymanager.queue.UserQuery;

import java.util.*;

@Component
public class AgentRegistry {

    HashMap<Agent, Query> agentRegistry = new HashMap<>();

    public void addAgent(Agent a) {
        agentRegistry.put(a, new NullQuery());
    }

    public Agent getFirstAvailable() {
        for (Agent key: agentRegistry.keySet()) {
            if (key.getLocation().equals("subAgent"))  {
                continue;
            } else {
                Object response = agentRegistry.get(key);
                if (response instanceof NullQuery) {
                    return key;
                }
            }
        }
        return null;
    }

    public void assignQuery(String s, UserQuery uq) throws AgentNotFoundException {

        synchronized (agentRegistry) {
            for (Agent key: agentRegistry.keySet()) {
                if (key.getIdentifier().equals(s)) {
                    agentRegistry.put(key, uq);
                    return;
                }
            }
        }
        throw new AgentNotFoundException("Failed to find agent with name " + s);
    }

    public UserQuery removeQuery(String s) throws AgentNotFoundException {
        synchronized (agentRegistry) {
            for (Agent key: agentRegistry.keySet()) {
                if (key.getIdentifier().equals(s)) {
                    UserQuery uq = (UserQuery) agentRegistry.get(key);
                    agentRegistry.put(key, new NullQuery());
                    return uq;
                }
            }
        }
        throw new AgentNotFoundException("Failed to find agent with name " + s);
    }

    public String checkProgress(String s) {
        Query q = null;
        synchronized (agentRegistry) {
            for (Agent key: agentRegistry.keySet()) {
                if (key.getIdentifier().equals(s)) {
                    q = agentRegistry.get(key);
                }
            }
        }
        if (q != null) {
            if (q instanceof UserQuery) {
                UserQuery uq = (UserQuery) q;
                long current = System.currentTimeMillis();
                long uqTime = uq.getTimestamp();
                return s + " " + (current - uqTime) + " millis since picking up query: " + uq.getRoom();
            } else if (q instanceof NullQuery) {
                return s + " is not current working on a query.";
            }
        }
        return s + " is not in the repository.";
    }

    /*
     * Shouldn't really be used due to issues with java.util.ConcurrentModificationException
     */
    private Set<Agent> getAgents() {
        return agentRegistry.keySet();
    }

    public Set<String> getAgentLocations() {
        Set<String> uris = new HashSet<String>();
        synchronized (agentRegistry) {
            for (Agent a: agentRegistry.keySet()) {
                uris.add(a.getLocation());
            }
        }
        return uris;
    }

    public HashMap<String,String> getAgentNameAndLocations() {
        HashMap<String,String> agents = new HashMap<>();
        synchronized (agentRegistry) {
            for (Agent a: agentRegistry.keySet()) {
                agents.put(a.getIdentifier(),a.getLocation());
            }
        }
        return agents;
    }

}
