package perfectfield.sim.querymanager.agents;

public class Agent {

    private String identifier; //Name
    private String location;

    public Agent(String identifier, String location) {
        this.identifier = identifier;
        this.location = location;
    }

    public Agent() {};

    public String getIdentifier() {
        return identifier;
    }

    public String getLocation() {
        return location;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setLocation(String location) {this.location=location;}

    @Override
    public String toString() {
        return identifier + ", " + location;
    }
}
