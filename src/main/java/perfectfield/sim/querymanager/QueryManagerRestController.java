package perfectfield.sim.querymanager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import perfectfield.sim.querymanager.agents.Agent;
import perfectfield.sim.querymanager.agents.AgentNotFoundException;
import perfectfield.sim.querymanager.agents.AgentRegistry;
import perfectfield.sim.querymanager.queue.CompletedQueue;
import perfectfield.sim.querymanager.queue.QueryBody;
import perfectfield.sim.querymanager.queue.QueryQueue;
import perfectfield.sim.querymanager.queue.UserQuery;
import perfectfield.sim.querymanager.restObjects.Path;
import perfectfield.sim.querymanager.restObjects.QueryResponse;

import com.google.gson.Gson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@EnableAsync
@RestController
public class QueryManagerRestController {

    Logger logger = LoggerFactory.getLogger(QueryManagerRestController.class);

    @Value("${server.url}")
    String appUrl;

    @Value("${test.location}")
    String testLoc;

    @Autowired
    AgentRegistry registry;

    @Autowired
    QueryQueue queue;

    @Autowired
    CompletedQueue completedQueue;

    Gson gson = new Gson();

    @PostMapping(path="/register",produces="application/json",consumes="application/json")
    public ResponseEntity<Object> registerAgent(@RequestBody Agent a) {
        logger.info("Incoming agent: " + a.toString());
        registry.addAgent(a);
        if (!queue.isEmpty()) {
            logger.info("There are items on the queue, so putting " + a.toString() + " to work straight away.");
            pickupUserQuery(a);
        }
        return ResponseEntity.ok(gson.toJson("Hello " + a.getIdentifier()));
    }

    @RequestMapping(value = "/ping", method = RequestMethod.GET)   
    public ResponseEntity<Object> ping() {
        logger.info("Incoming ping");
        return ResponseEntity.ok().build();
    }

    @PostMapping(path="/submitQuery",consumes = "application/json", produces = "application/json")
    @CrossOrigin //Need this to test locally
    public ResponseEntity<String> registerUserQuery(@RequestBody UserQuery uq) {
        queue.add(uq);
        logger.info("Registered query: " + uq);
        //Now send to agent.
        Agent availableAgent = registry.getFirstAvailable();
        if (availableAgent == null) {
            logger.info("No available agents for query");
        } else {
            String agentName = availableAgent.getIdentifier();
            try {
                registry.assignQuery(agentName, uq);
                logger.info("Assigned " + agentName + " to " + uq.toString());
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.postForLocation(availableAgent.getLocation()+"/query", new QueryBody(uq.getRoom()));
            } catch (AgentNotFoundException | Exception e) {
                queue.add(uq);
                logger.info("Failed to post query to " + agentName + ", returning query to queue.");
                return ResponseEntity.badRequest().build();
            }
        }

        return ResponseEntity.ok(gson.toJson("Query recieved"));
    }

    /*
     * put, not get, as needs to get the agent details
     */
    @PutMapping(path="/pickupQuery",consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> pickupUserQuery(@RequestBody Agent a) {
        UserQuery uq = queue.get();
        String agentName = a.getIdentifier();
        //Do this check first, don't want to to assign null query to agent.
        if (uq == null) {
            logger.info("Queue is empty, agent " + agentName + " has nothing.");
            return ResponseEntity.badRequest().build();
        }

        try {
            registry.assignQuery(agentName, uq);
            logger.info("Assigned " + agentName + " to " + uq.toString());
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.postForLocation(a.getLocation()+"/query", new QueryBody(uq.getRoom()));
        } catch (AgentNotFoundException ae) {
            //re-add uq to queue so don't loose it
            queue.add(uq);
            logger.info("No agent with name " + agentName + ", returning query to queue.");
            return ResponseEntity.badRequest().build();
        } catch (Exception e) {
            queue.add(uq);
            logger.info("Failed to post query to " + agentName + ", returning query to queue.");
            return ResponseEntity.badRequest().build();
        }

        //Return ok
        return ResponseEntity.ok(gson.toJson("Query assigned"));
    }

    /*
     * Incoming message from agent. The Agent Name is the identifier.
     */
    @PostMapping(path="/queryResponse",consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object>  receiveQueryResponse(@RequestBody QueryResponse qr) {
        String agentName = qr.getIdentifier();
        String response = qr.getResponse();
        try {
            logger.info("Agent " + agentName + " completed query, response: " + response);
            UserQuery uq = registry.removeQuery(agentName);
            uq.setDirections(response);
            completedQueue.add(uq);

        } catch (AgentNotFoundException ae) {
            logger.info("No agent with name " + agentName + ", can't unassign.");
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().build();
    }
}
