package perfectfield.sim.querymanager.queue;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CompletedQueue {

    private List<CompletedQueueItem> entries = new ArrayList<>();
    //TODO: when gets to a certain size, write to DB

    public void add(UserQuery uq) {
        Date d = new Date(uq.getTimestamp());
        entries.add(new CompletedQueueItem("Query at " + d + ": find " + uq.getRoom() + ", Response: "+uq.getDirections()));
    }

    public List<CompletedQueueItem> getView() {
        return entries;
    }
}
