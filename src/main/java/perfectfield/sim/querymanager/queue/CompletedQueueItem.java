package perfectfield.sim.querymanager.queue;

public class CompletedQueueItem {
    String description;

    public CompletedQueueItem(String description) {
        this.description = description;
    }

    public String getDescription() {return description;}  
    
    @Override
    public String toString() {
        return description;
    }
}
