package perfectfield.sim.querymanager.queue;

import org.springframework.stereotype.Component;
import perfectfield.sim.querymanager.view.QueueView;

import java.util.*;

@Component
public class QueryQueue {

    private Queue<UserQuery> queryQueue = new LinkedList<>();

    public void add(UserQuery uq) {
        synchronized (queryQueue) {
            queryQueue.add(uq);
        }
    }

    public UserQuery get() {
        synchronized (queryQueue) {
            UserQuery uq = queryQueue.peek();
            if (uq != null) {
                queryQueue.remove(uq);
            }
            return uq;
        }
    }

    public boolean isEmpty() {
        return queryQueue.isEmpty();
    }

    //TODO: will be a problem when scale system, as locks query queue.
    public List<QueueView> getView() {
        List<QueueView> qv = new ArrayList<>();
        synchronized (queryQueue) {
            for (UserQuery uq: queryQueue) {
                String path = "Path not found";
                if (uq.getDirections() != null) {
                    path = uq.getDirections();
                }                
                qv.add(new QueueView(uq.getTimestamp(), uq.getRoom(), uq.getLocation(), path));
            }
        }
        return qv;
    }
}
