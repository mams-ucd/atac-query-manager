package perfectfield.sim.querymanager.queue;

public class UserQuery extends Query {

    private long timestamp;
    private String room;
    private String location;
    private String directions;

    public UserQuery(String room, String location) {
        this.room = room;
        this.location = location;
        this.timestamp = System.currentTimeMillis();
    }

    public UserQuery() {this.timestamp = System.currentTimeMillis();};

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {this.timestamp = timestamp;}

    public String getLocation() {return location;}

    public void setRoom(String room){this.room = room;}

    public String getRoom() {return room;}

    public String getDirections() {return directions;}

    public void setDirections(String directions) {this.directions = directions;}
    
   @Override
    public String toString() {
        return "Query from " + location + ": " + room;
    }
}
