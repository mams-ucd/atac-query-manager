package perfectfield.sim.querymanager.queue;


public class QueryBody {
    //Dates
    private String room;

    public QueryBody() {

    }
    
    public QueryBody(String room) {
        this.room = room;
    }

    public String getRoom() {
        return room;
    }
    public void setRoom(String room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return "Looking for " + room;
    }
}
