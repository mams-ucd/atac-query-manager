package perfectfield.sim.querymanager.view;

import java.util.Date;

public class QueueView {

    private Date time;
    private String queryBody;
    private String location;
    private String response;

    public QueueView(long timestamp, String queryBody, String location, String response) {
        this.queryBody = queryBody;
        this.location = location;
        this.time = new Date(timestamp);
        this.response = response;
    }

    public String getQueryBody() {
        return queryBody;
    }

    public void setQueryBody(String queryBody) {this.queryBody = queryBody;}

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {this.time = time;}

    public String getLocation() {return location;}

    public void setLocation(String location) {this.location = location;}

    public String getResponse() {return response;}

    public void setResponse(String response) {this.response = response;}

}
