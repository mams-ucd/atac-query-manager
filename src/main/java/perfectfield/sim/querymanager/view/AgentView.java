package perfectfield.sim.querymanager.view;

public class AgentView {

    String name;
    String location;
    String query;

    public AgentView(String name, String location, String query) {
        this.name = name;
        this.location = location;
        this.query = query;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getQuery() {
        return query;
    }

}
