package perfectfield.sim.querymanager.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import perfectfield.sim.querymanager.QueryManagerRestController;
import perfectfield.sim.querymanager.agents.AgentRegistry;
import perfectfield.sim.querymanager.queue.CompletedQueue;
import perfectfield.sim.querymanager.queue.QueryBody;
import perfectfield.sim.querymanager.queue.QueryQueue;
import perfectfield.sim.querymanager.queue.UserQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class QueryManagerViewController {

    Logger logger = LoggerFactory.getLogger(QueryManagerViewController.class);

    @Autowired
    QueryManagerRestController restController;
    
    @Value("${server.url}")
    String loc;

    @Autowired
    AgentRegistry registry;

    @Autowired
    QueryQueue queue;

    @Autowired
    CompletedQueue completedQueue;

    @GetMapping(value={"/","/overview"})
    public String getQueryManagerVisual(Model model) {
        
        List<AgentView> agentViews = buildAgentViews();
        QueryBody qb = new QueryBody();
        model.addAttribute("query", qb);
        
        model.addAttribute("agentCount", agentViews.size());
        model.addAttribute("agentViews", agentViews);
        List<QueueView> qv = queue.getView();
        model.addAttribute("queueCount", qv.size());
        model.addAttribute("queueView", qv);
        model.addAttribute("completedQueueViews", completedQueue.getView());

        return "queryManager";
    }

    private List<AgentView> buildAgentViews() {
        HashMap<String,String> agents = registry.getAgentNameAndLocations();
        List<AgentView> agentViews = new ArrayList<>();
        for (String name: agents.keySet()) {
            String location = agents.get(name);
            String progress = registry.checkProgress(name);
            agentViews.add(new AgentView(name, location, progress));

        }
        return agentViews;
    }

    @PostMapping("/addQuery")
    public String addMockQuery(@ModelAttribute QueryBody qb) {
        UserQuery userQuery = new UserQuery(qb.getRoom(), loc);
        restController.registerUserQuery(userQuery);

        return "redirect:/";
    }

}
