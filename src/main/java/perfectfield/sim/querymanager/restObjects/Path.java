package perfectfield.sim.querymanager.restObjects;

public class Path {
    
    String incomingDirection;
    String roomName;

    public Path() {

    }

    public String getIncomingDirection() {
        return incomingDirection;
    }
    public String getRoomName() {
        return roomName;
    }
    public void setIncomingDirection(String incomingDirection) {
        this.incomingDirection = incomingDirection;
    }
    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
    
}
