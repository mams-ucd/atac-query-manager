package perfectfield.sim.querymanager.restObjects;


/*
 * POJO for /queryResponse responsebody to map to.
 * When incoming from an agent, the identifier is the agent name.
 * Also used as outgoing object to client, where identifier is the query token
 */
public class QueryResponse {

    private String identifier; //agent name
    private String response;
    
    public QueryResponse () {}

    public QueryResponse(String identifier, String response) {
        this.identifier = identifier;
        this.response = response;
    }

    public String getIdentifier() { return identifier; }
    public String getResponse() { return response; }

    public void setIdentifier(String identifier) { this.identifier=identifier; }
    
    public void setResponse(String response) { 
        this.response = response;
    }

    @Override
    public String toString() {
        return identifier + ":" + response;
    }
}
