package perfectfield.sim.querymanager.restObjects;

public class Notification {
    public String notification;
    public String location;

    public Notification() {};
    public Notification(String n, String l) {
        this.notification = n;
        this.location = l;
    }
    public void setNotification(String n) {this.notification = n;}
    public void setLocation(String n) {this.location = n;}
}
