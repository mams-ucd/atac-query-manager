package perfectfield.sim.querymanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QueryManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(QueryManagerApplication.class, args);
    }
    
    //Need an endpoint where agents can register.
    //Then when get a user query, send notification to agent(s).
    //Agent "decides" whether to accept or not, and queries QM to get user query
    //QM marks as in progress. Marks as done when Agent sends answer.

}
